﻿using System;

namespace LibSHOUTStr
{
    public class StringLibrary
    {
        public static string StringToUpper(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return "Entrez une chaîne";
            }

            return str.ToUpper();
        }
    }
}
