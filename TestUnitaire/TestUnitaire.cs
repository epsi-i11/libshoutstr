using System;
using Xunit;
using LibSHOUTStr;

namespace TestUnitaire
{
    public class TestUnitaire
    {
        [Fact]
        public void TestLibShoutSTR()
        {
            string test = "test";
            string res = StringLibrary.StringToUpper(test);
            Assert.True(res=="TEST");
        }

    }
}
